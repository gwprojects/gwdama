gwdama.io
=========
This submodule contains the basic classes for Input-and-Output of data.

:Dataset:
    Basically, this is an enhanced version of the `h5py.Dataset class <http://docs.h5py.org/en/stable/high/dataset.html>`_.
    This is the basic object to store data with attributes. It will also contain some
    metods to plot its content and do some basic data analysis (to be added soon).
    
:GwDataManager:
    Basic class to read and manipulate GW data. It contains all the methods
    needed to import data from vasious locations (online gwosc, local cvmfs,
    Virgo farm).


Data Managment
---------------

:ref:`gwdatamanager:GwDataManager` object can be filled with data in three different ways, depending on the combination of parameters ``mode`` and ``storage`` and the name given to the object, in the case this is a an existing ``hdf5`` file, for example saved with this package.

.. tabularcolumns:: |c|c|l|

+------------------+---------+---------------------------------------------------------------+
|  mode            | storage | GwDataManager behaviour                                       |
+==================+=========+===============================================================+
| ``w`` (default), | ``mem`` | Data are stored in RAM. It must be saved otherwise they will  |
|                  |         | be authomatically deleted when the Python kernel is shut down |
| ``w-``, ``x``    +---------+---------------------------------------------------------------+
|                  | ``tmp`` | It creates on disk a temporary file (somwhere, typically in   |
|                  |         | ``\tmp`` which is delated after the kernel is shut down).     |
|                  |         | Remember to write it on disk when you are done                |
+------------------+---------+---------------------------------------------------------------+
| ``r``, ``r+``,   | Unused  | It attempts to read the file passed as e name for the         |
| ``a``            |         | GwDataManager object. In case of ``a``, it also alloes to     |
|                  |         | create it if not existing. Then, the object is saved on disk  |
|                  |         | and must be closed  (``.close()`` method) before leaving      |
+------------------+---------+---------------------------------------------------------------+
    
``mem`` is expected to be faster but imited by the RAM of the machine. ``tmp`` allows to temporary store larger files on disk (which must be saved, otherwise they get lost). ``a`` allows to create and access a new file on disk. Beware that in the latter case the file **must be closed** at the end of the script. Another good idea is to make use of a context manager by means of a ``with`` statement:
::

    >>> with GwDataManager('file.h5',mode='a') as dama:
    >>>     ...


The basic data format to contain the data is a `h5py.Dataset like <http://docs.h5py.org/en/stable/high/dataset.html>`_ object. These :ref:`dataset:Dataset` are created within an instance of :ref:`gwdatamanager:GwDataManager` with the method ``create_dataset(name, shape, dtype)``. They contain data, typically of numeric type but also strings and other objects in case, and some attributes (or metadata). For example, for GW data, and in general all time series, it is important the information of when they have been recorded, and at which sampling frequency. A neme and a unit are also usefull. This can be conveniently added and customised. Also a :ref:`gwdatamanager:GwDataManager` object contains attributes for itself. Datasets can be also collected into `Groups <http://docs.h5py.org/en/stable/high/group.html>`_. 

When the data have been acquired and possibly pre-processed, they can be saved to ``hdf5`` format, and later read back. Refer to the :ref:`quick:Quick start guide` for furhter examples, or to the :ref:`dataset:Dataset` and :ref:`gwdatamanager:GwDataManager` classes documentation.
