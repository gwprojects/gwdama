Preprocessing
=============

.. automodule:: gwdama.preprocessing.preprocessing

Spectral Analysis
-----------------

.. autofunction:: gwdama.preprocessing.preprocessing.bandpass

.. autofunction:: gwdama.preprocessing.whiten

.. autofunction:: gwdama.preprocessing.mtm_psd

.. autofunction:: gwdama.preprocessing.avg_spectro

.. autofunction:: gwdama.preprocessing.multi_spectro

.. autofunction:: gwdama.preprocessing.mtm_spectro


Miscellanea
-----------

.. autofunction:: gwdama.preprocessing.decimate_recursive

.. autofunction:: gwdama.preprocessing.next_power_of_2

.. autofunction:: gwdama.preprocessing.taper
