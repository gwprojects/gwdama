Dataset
-------

**Datasets** are the basic unit to store data. 

.. class:: Dataset (identifier)

    This is an *enhanced* version of the `h5py.Dataset class <http://docs.h5py.org/en/stable/high/dataset.html>`_,
    with the addition of some custom methods and attributes to facilitate its manipulation. Basically, this is similar
    to a Python :func:`dict`:, with a "key" to identify and access it within a :py:class:`gwdama.io.GwDataManager` object
    (see the example), and an associated value, which can be set in the usual way of dictionaries, and called back with the class ``.data`` attribute.   
    
    As for dictionaries, the data corresponding to a :ref:`Dataset` can be pretty general. Certain class methods however are callable only
    if the data has a specific format. For example, the ``duration`` property is callable only for  :ref:`Dataset`  objects with a ``sample_rate``
    attribute (more details momentarily).
    
    In addition with respect to dictionaries, these obkectys can have `attributes of h5py <https://docs.h5py.org/en/stable/high/attr.html>`_. These are
    meant to make each object "self-descibing". See the examples on how to access, define or modify these attributes.
    
    **Attributes:**

    .. py:attribute:: attrs
    
        attributes of this dataset. Refer to the examples below or to the discription of `h5py attributes <https://docs.h5py.org/en/stable/high/attr.html>`_ for more details

    .. py:attribute:: data

        returns the content of the corresponding object as a :func:`numpy.array`. If the data is of not numeric type, like a ``str``, it will return an array of `dtype=object`
        
    .. py:attribute:: dtype

        attribute dtype, similar to the :func:`numpy.dtype` class . Refer to the `various numpy data type objects <https://numpy.org/doc/stable/reference/arrays.dtypes.html>`_
        
    .. py:attribute:: name 

        string giving the full path to this dataset
        
    .. py:attribute:: parent

        returns the `h5py.Group instance <https://docs.h5py.org/en/stable/high/groups.html>`_ containing this dataset. This can be used to create other datasets next to the current one

    .. py:attribute:: shape

        attribute shape, similar to that of a :func:`numpy.array`
    
    .. py:attribute:: show_attrs
    
        prints in a convenient way all the attributes (metadata) of the Dataset object
        
    .. py:attribute:: size

        attribute size, similar to that of a :func:`numpy.array`

        
    **Methods:**  
    
    .. autofunction:: gwdama.io.dataset.duration
    
    .. autofunction:: gwdama.io.dataset.hist       

    .. autofunction:: gwdama.io.dataset.psd       
        
    .. autofunction:: gwdama.io.dataset.resample
    
    .. autofunction:: gwdama.io.dataset.taper
    
    .. autofunction:: gwdama.io.dataset.to_TimeSeries
    
    .. autofunction:: gwdama.io.dataset.whiten
        
    **Example:**

    Typically, this is created within a :py:class:`gwdama.io.GwDataManager` object with the following methods::
    
      >>> dama = GwDataManager()
      >>> dama.create_dataset('dset_name', data=[1,2,3,4])
        
    It can be assigned to a variable::
    
      >>> dset = dama["dset_name"]
        
    and its content and attributes returned by means of::
    
      >>> print(dset.data) 
      >>> dset.show_attrs
        
    To change the value corresponding to a ``Dataset``, or to create it if it doesn't exist::
    
      >>> dama['new_dset'] = [5,6,7,8]       
 
