===================
 Quick start guide
===================

This guide will walk the reader through the main operations that can be done with :ref:`index:GWdama`, from installation to basic dataset creation and manipulation.

--------------
 Installation
--------------

:ref:`index:GWdama` can be easily installed via `pip <https://docs.python.org/3/installing/index.html>`_:

.. code-block:: console

    $ pip install gwdama

and requires Python 3.6.0 or higher. The previous command authomatically fulfill all the required dependencies (like on :py:mod:`numpy` or :py:mod:`matplotlib`), so you are ready to start generating datasets and making plots.

It can be used programmatically from terminal or within a python script:
::

    >>> from gwdama.io import GwDataManager

Refer to the :doc:`full documentation <installation>` for further details.


.. topic:: Virtual Environments  

    It is usually recommended to perform the previous installation within a `python virtual environment <https://docs.python.org/3.6/tutorial/venv.html>`_.
    Assuming you want to name the environment ``myenv`` (otherwise substitute the name of your choice in the following lines), 

    .. code-block:: console

        $ python3 -m venv myenv
        $ source myenv/bin/activate
    
    and the indication ``(myenv)`` will appear on the left of your terminal. It is good practice now to update pip before proceeding to install ``gwdama``:

    .. code-block:: console
    
        (myenv) $ pip install --upgrade pip

    Then, type the command at the beginning of this section and you are ready to go!
    


--------------------
 Creating a dataset
--------------------

A dataset of, say, random numbers can be created as:
::

    >>> from gwdama.io import GwDataManager
    >>> import numpy as np
    
    >>> dama = GwDataManager("my_dama")
    >>> dama.create_dataset('random_n', data=np.random.normal(0, 1, (100,)))
    >>> dama.create_dataset('a_list', data=[1, 2, 3, 4])
    >>> dama.create_dataset('a_string', data="this is a string")
    
Then, we can show the contet of this object by:
::

    >>> print(dama)
    my_dama:
      ├── a_list
      ├── a_string
      └── random_n

      Attributes:
         dama_name : my_dama
        time_stamp : 20-07-28_19h36m47s
    
Other attributes can be added to both the :ref:`gwdatamanager:GwDataManager` and the :doc:`Datasets <dataset>` therein:
::

    >>> dama.attrs['owner'] = 'Francesco'
    >>> dama.show_attrs
    my_dama:
      ├── a_list
      ├── a_string
      └── random_n

      Attributes:
         dama_name : my_dama
             owner : Francesco
        time_stamp : 20-07-28_19h36m47s  
        
Same thing is true for datasets. These can be accessed from their keys, with the same syntax of a dictionsry:
::

    >>> dset = dama['random_n']
    >>> dset.attrs['t0'] = 0             # It is conveninet to use gps times
    >>> dset.attrs['sample_rate'] = 10   # measured in Hz
    
    >>> dset.show_attrs
    sample_rate : 10
             t0 : 0

.. note:: Notice that the keyword ``'sample_rate'`` is the standard way to indicate the sampling frequency of a time series in :ref:`index:GWdama`. Although every other label can be used to indicate the same property, some methods (like ``.duration()`` or ``.psd()``) rely on this specific attribute to make their calculations. Of course, the behaiviour of all of them can be altered suggesting the correct sampling frequency by assigning this to the method attribute ``fs``.

To get back the data contained in this dataset, call the attribute ``.data``::

    >>> dset.data
    array([ 0.33441701, -0.82764243, -0.04547589,  0.90735673,
           ...
           -0.02077637,  2.07473129,  0.72205023,  0.50083398])

.. note:: Output of random number generator may vary depending on the `random seed <https://numpy.org/doc/stable/reference/random/generated/numpy.random.seed.html?highlight=seed#numpy.random.seed>`_. It is always a good idea to set it before generating anything random with ``np.random.seed(1234)``.

Alternatively, you can use the `default ways of h5py.Datasets <https://docs.h5py.org/en/stable/high/dataset.html#reading-writing-data>`_, such as ``dset[:]``, ``dset[...]`` or ``dset[()]``, which also allows to modify its data content. 

.. warning:: If you try to modify the dataset via its attribute ``.data`` you get:
  ::

    >>> dset.data = 0
    AttributeError: can't set attribute
  
----------------------------------
 Basic plotting and data analysis
----------------------------------

:ref:`index:GWdama` contains some functions aimed at data visualisation and data analysis (in continuous expansion!!). These can be applied on numeric data only, in particular in the form of a 1D array, such as a time series. Of course, the `numpy.ndarray <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`_ returned by the ``.data`` attribute of each :doc:`dataset <dataset>` possess all the methods and attributes of any Numpy array; refer to the corresponding documentation for further details.

Simple plots
------------

The data contained in :doc:`Dataset objects <dataset>` can be readily plotted (whitout pretense)::

  >>> dset = dama['random_n']    # This is the dataset created in the previous example with 100 random numbers
  
  >>> plt.plot(dset)
  >>> plt.show()

.. image:: figures/simple1.png
   :height: 300px
   :width: 400 px
   :alt: random
   :align: center
   
Refer to the next examples for more advanced visualisation operations.

.. note:: If the data type is a `structured array <https://numpy.org/doc/stable/user/basics.rec.html#module-numpy.doc.structured_arrays>`_, as in the case of the data returned by the :py:meth:`~gwdama.io.Dataset.psd` method of :doc:`dataset` (refer to :ref:`the next example<PSD>`), the various *names* of the *fields* should be spelled out.


Histograms
----------

One basic operation of such a kind is producing the **histogram** of the data in a :doc:`dataset object <dataset>`. There is a dedicated method to do that, based on the `pyplot.hist class <https://matplotlib.org/3.3.1/api/_as_gen/matplotlib.pyplot.hist.html>`_, that returns a  `Figure object of matplotlib <https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.pyplot.figure.html>`_::

  >>> dset.hist()
  
.. image:: figures/hist1.png
   :height: 300px
   :width: 400 px
   :alt: histogram
   :align: center


The resulting `Figure object <https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.pyplot.figure.html>`_ can be also associated to a variable, which in turn allows to modify and ``.reshow()`` (similar to the ``.show()`` method but for closed figures)::

  >>> plot = dset.hist()
  >>> ax = plot.gca()                  # Get access to the Axes instance
  
  >>> ax.set_xlabel("Random numbers")  # New xaxis label
  >>> ax.patches[-1].set_label('Data') # New label for the previously made histogram

Fit a normal distribution to the data::

  >>> from scipy.stats import norm
  >>> mu, std = norm.fit(dset.data)

Plot the p.d.f.::

  >>> xmin, xmax = ax.get_xlim()
  >>> x = np.linspace(xmin, xmax, 100)
  >>> p = norm.pdf(x, mu, std)
  >>> ax.plot(x, p, 'r--', linewidth=1, label='Gaussian fit')
  >>> ax.legend()

  >>> plot.reshow()                    # New method to re-show closed figures

.. image:: figures/hist2.png
   :height: 300px
   :width: 400 px
   :alt: histogram
   :align: center
   

.. _PSD:
   
Power Spectral Density
----------------------

The `Power Spectral Density <https://en.wikipedia.org/wiki/Spectral_density>`_ is one of the most important mathematical instrument for signal processing. :ref:`index:GWdama` has a method to compute it, based on `scipy.signal.welch function <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.welch.html>`_. 

Let us make a brend new "dama" with (the realisation of) a *white noise process*::

  >>> dama = GwDataManager('Signals')

  >>> np.random.seed(1234)
  >>> fs = 2048              # sampling frequency
  >>> N = 1e5                # number of points
  >>> time = np.arange(N)/fs # reference time array

  >>> white = dama.create_dataset('White', data = np.random.normal(0,1, size=time.shape))
  
Since it is not particularly interesting to study Gasussian data, let us add a *monocromatic signal* into noise, and a (rel. of a) coloured noise process::

  >>> freq = 500
  >>> line  = dama.create_dataset('Line', data = white.data + np.sin(2*np.pi*freq*time))
  
  >>> red   = dama.create_dataset('Red', data = white[1:]+white[:-1])  # Red noise for cheap

The ``.psd()`` method of :doc:`dataset objects <dataset>` can give two different kind of outputs: if the argument ``return_type`` is ``'array'`` it will return the usual output of the `scipy.signal.welch function <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.welch.html>`_, if it is ``'dataset'`` (default), it will make a new dataset along the current one, with frequency and PSD data in the form of a `structured array <https://numpy.org/doc/stable/user/basics.rec.html#module-numpy.doc.structured_arrays>`_. We will proceed with the latter, but firstly we must add the the ``'sample_rate'`` attribute to the previous datasets (or pass ``fs`` directly to the ``.psd()`` method)::

  >>> for ds in dama.values():
          ds.attrs['sample_rate'] = fs
          ds.psd(2,1)                   # fftlength = 2 seconds, overlap = 1 second
          
  >>> print(dama)
  Signals:
  ├── Line
  ├── Line_psd
  ├── Red
  ├── Red_psd
  ├── White
  └── White_psd

  Attributes:
     dama_name : Signals
    time_stamp : 20-11-11_19h29m40s

.. note:: Notice that the :py:func:`~gwdama.io.dataset.psd` method of :doc:`Datasets <dataset>` allows to specify alkso a ``dts_kay`` parameter for the newly created PSD dataset. For example, with:
    ::
      
      >>> ds.psd(2,1, dts_key='PSDs/'+ds.name.split('/')[-1])
    
    you would have created a new Gorup, named ``'PSDs'``, with the PSDs of the verious datasets labeled with their original names. In general, all methods that allow to create a new dataset have this attribute for specifying its name.


Finally, let's visualise what we got::

  >>> fig, ax = plt.subplots()
  >>> for name, ds in dama.items():
         if name.endswith('_psd'):
             ax.semilogy('freq', 'PSD', data=ds, alpha=.7, label=name)

  >>> ax.legend()
  >>> ax.set_ylim(1e-5,1)
  >>> ax.set_xlabel('Frequency [Hz]')
  >>> ax.set_ylabel('PSD [1/Hz]')
  >>> plt.show()

.. image:: figures/PSD1.png
   :height: 300px
   :width: 400 px
   :alt: PSD
   :align: center

.. note:: In the previous example, we have passed to `pyplot.plot() <https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.plot.html>`_ function the "labels" of the *structured object* ``ds``: ``plt.plot('label1', 'label2', data=obj, ...)``. This is because the data corresponding to ``ds`` is a  `structued array <https://numpy.org/doc/stable/user/basics.rec.html#module-numpy.doc.structured_arrays>`_, where various `numpy.array <https://numpy.org/doc/stable/reference/generated/numpy.array.html>`_ are organized into *fields*, whose *names* are in this case ``'freq'`` and ``'PSD'`` (with their obvious meaning). This is similar to a `dict <https://docs.python.org/3/library/stdtypes.html#dict>`_ or a `pandas.DataFrame <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html#pandas.DataFrame>`_. You can check this by typing::
  
    >>> dama['White_psd'].data.dtype
    dtype([('freq', '<f8'), ('PSD', '<f8')])
  
  which tells us that the previous data are both of `float64 type <https://numpy.org/doc/stable/reference/arrays.dtypes.html>`_, or ``dama['White_psd'].data.dtype.names`` to get only the field names.
  
  Alternatively, one could have spelled out the various parts passing the names directly to the structured object, that is: ``ax.semilogy(ds['freq'], ds['PSD'], alpha=.7, label=name)`` would have produced the exact same result. Beware however not to confuse ``ds``, which is a :doc:`Datasets <dataset>`, with a :ref:`Group`  or :ref:`gwdatamanager:GwDataManager` object, where the *label* inside braces is the key to a :doc:`Datasets <dataset>` or a sub-:ref:`Group`.


------------------------------
 Writing and reading datasets
------------------------------

Now it is time to **write your data** to disc:
::

    >>> out_f = 'out_dataset.h5'
    >>> dama.write_gwdama(out_f)
    
Then remember to **close your previous file** before leaving the session:
::

    >>> dama.close()
    >>> del dama       # Redundant but usefull

.. note:: The previous operation is automatically performed every time the session is closed. However, it is good practice to do this manually every time there is no more need of a certain variable.

To **read data** back, you simply need to pass a valid path to an hdf5 file, such as the one previously saved, as the main argument of the :ref:`gwdatamanager:GwDataManager` object initialiser. This substitute its ``dama_name`` attribute, which will be authmatically imported from the
file we are about to read. Notice also the possible options provided by the ``mode`` and ``storage`` parameters, as described in the  :ref:`package:Data managment` section or in the :doc:`package description <gwdatamanager>`:
::

    >>> new_dama = GwDataManager(out_f)
    Reading dama
    >>> print(new_dama)
    my_dama:
      ├── a_list
      ├── a_string
      └── random_n

      Attributes:
         dama_name : my_dama
             owner : Francesco
        time_stamp : 20-07-30_12h19m32s

As you can see, you recover the data and metadata from the previusly saved ``dama``. 


----------------
 Read open data 
----------------

Open data can be accessed from both online and local virtual disks. Refer to the `GW Open Science Center (GWOSC) documentation <https://www.gw-openscience.org/about/>`_ to check which time segmts are available.

From online GWOSC
-----------------

This is the standard approach, where data is fethced online and directly imported to the curent instasnce of :ref:`gwdatamanager:GwDataManager`. If you plan to import more of them into the same ``dama``, don't forget ot associate a key to each of them::

    >>> from gwpy.time import to_gps                        # Usefull to convert dates to gps times    
    >>> e_gps = to_gps("2017-08-14 12:00")

    >>> dama = GwDataManager()                              # Default name 'mydama' assigned to the dictionary

    >>> dama.read_gwdata(e_gps - 50, e_gps +50, ifo='L1',   # Required params
                         m_data_source="gwosc-remote",      # data source
                         dts_key='online')                  # Optional but useful for giving names to things


From local CVMFS
----------------
 
CernVM-FS must be installed and configured on your computer. Refer to its `description on the GWOSC website <https://www.gw-openscience.org/cvmfs/>`_ or to `this Quick start guide <https://cernvm.cern.ch/portal/filesystem/quickstart>`_.

Assuming your data is mapped to the following path (which you can always modify passing the correct path to this argument of the ``.read_gwdata()`` method)::

   cvmfs_path = '/data2/cvmfs/gwosc.osgstorage.org/gwdata/' 

data can be read with:

::

    >>> start = '2017-06-08 01:00:00'  # starting time
    >>> end   = '2017-06-08 02:00:00'  # ending time
    >>> ifo   = 'H1'                   # which interfereometer

    >>> rate='4k'                      # (optional) determines the sample rate
    >>> frmt='hdf5'                    # and the format of the data (gwf or hdf5)
    
    >>> dama.read_gwdata(start, end, m_data_source="gwosc-cvmfs", ifo=ifo, m_data_format=frmt)