========
 GWdama
========

This package aims at providing a unified and easy to use interface to access Gravitational Wave (GW) data and output some *well organised* datasets, ready to be used for Machine Learning projects or Data Analys purposes (source properties, noise studies, etc.). This is also sufficiently versatile to be used with any other kind of data.  

---------------------------
 Data Preparation Workflow
---------------------------

The typical use case of this package  concerns data preparation and can be viewed, in a *pipeline*, as the preliminary stage for Data Analysis. Although it is primarily meant for GW analyses, it is built to be sufficiently general to handle any data type. The basic workflow that can be implemented is the following:

1. **Data acquisition**: Data from GW detectors can be read from different locations, such as from *local* `frame files (gwf) <https://lappweb.in2p3.fr/virgo/FrameL/>`_, or from datasets previously saved with it, and especially from the `Gravitational Wave Open Science Center <https://www.gw-openscience.org/>`_ (GWOSC).

2. **Organisation into groups**: Various *acquisition channels*, data *epochs*, or preliminary preprocessing operations, can be organised in a hierarchical way in various `groups and subgroups <http://docs.h5py.org/en/stable/high/groups.html>`_, each containing their own metadata;

3. **Data preparation/preprocessing**: Along with the original "raw" data, one can perform several pre-processing operations and data manipulations, included in this package, in order to make the dataset ready for the following Data Analysis stage of the pipeline. This processed data is conveniently organised into new groups, and the "raw" ones can be removed to save memory;

4. **Reading and Writing**: Once the dataset for a specific task has been created, this can be saved to disk into `hdf5 format <https://www.hdfgroup.org/solutions/hdf5/>`_, preserving all the hierarchical group and sub-group structure and the metadata. This can be read back by **GWdama** for further data manipulation and preparation.


------------------
 Package overview
------------------

The class :ref:`dataset:dataset` provides the basic unit to store data. This is based on the `h5py.Dataset class <http://docs.h5py.org/en/stable/high/dataset.html>`_, whith the addition of some methods and attributes particularly useful for data manipulation and preprocessing. Basically this consisits of the "data" itself (typically but not exclusively of numeric format), acccessible from the ``.data`` property, and a series of metadata organised into  `h5py.Attributes class <http://docs.h5py.org/en/stable/high/attrs.html>`_. SOme pre-processing methods can be called form this class, and they can be chosen to return their output as an arrya  (or the typical format of the corresponding functions) or as a new dataset along with the current one.

:doc:`Datasets <dataset>` are organised into `groups <http://docs.h5py.org/en/stable/high/group.html>`_ in a hierarchical order. Also groups may have their own attributes. The global container of them is provided by the main  :ref:`gwdatamanager:GwDataManager` class, which constitutes the core of this package. This is a modified version of the `h5py.File class <http://docs.h5py.org/en/stable/high/file.html>`_. It provides the main interface to acces and store data. 

--------
Contents
--------

.. toctree::
   :maxdepth: 2
   
   quick
   installation
   package
   dataset
   gwdatamanager
   preprocessing
   examples

------------------
Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
