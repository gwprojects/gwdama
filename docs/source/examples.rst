==========
 Examples
==========

:class:`~gwdama.io.GwDataManager` class is meant to read, store and manipulate in a convenient way data of various types and from various sources. In the next sections we will see some examples of applications of this for purposes related to Data Analysis and Detector Characterization in the context of Gravitational Wave research. 

-----------------------------
 Dataset creation: use cases
-----------------------------

This section contains a list of possible use cases of :ref:`index:GWdama` for the creation of a dataset. In :ref:`quick:Creating a dataset`, we have examined the case of a dataset filled with random data. This example can be extended to cases where the data is available from different means, *e.g.* stored in a local file, or as the output of a preceeding pre-processing stage. Here, in the first two examples, we consider the acquisition of GW data as stored in one or more **channels**, during one or multiple time intervalse named "epochs".

Refer to the :ref:`next section <data-analysis>` for some basic examples of data analysis and visualization.

One channel, different epochs
=============================

In this case, one typically wants to investigate different features, occurring at different epochs, of one specific channel (or a limited number of them). This is the classical situation in which one wants to perform a *glitch* study.

Let's say that we have a CSV file with the specs of the glitches we want to investigate, for example::

    >>> from gwdama.io import GwDataManager
    >>> import pandas as pd
    
    >>> csv_path = '../data/glitch_sample_list_L1_O2.csv'
    >>> specs_df = pd.read_csv(test_csv)
    >>> specs.head()

.. csv-table:: Example of glitch specs from GravitySpy
   :header: , GPStime,peakFreq,snr,amplitude,centralFreq,duration,  bandwidth , ifo ,           label  
   :widths: auto
   :header-rows: 0
   :stub-columns: 1
 
   0, 1.182836e+09,   137.711,  56.263,  7.410000e-22, 3704.955,  1.500, 7374.007800    ,H1 , Repeating_Blips  
   1, 1.186798e+09,    65.438,  22.602,  3.450000e-22,    2669.345,     2.375,   5233.773800 ,    H1  , Power_Line  
   2, 1.175571e+09  ,  65.438 , 17.940  ,1.490000e-22   ,  2837.654 ,    2.250, 5570.391700 ,    H1  , Power_Line 
   3,  1.164876e+09 , 1075.609 ,17.353, 3.330000e-22,     4382.108,     0.809,7136.748535, H1  , 1080Lines 
   4 , 1.167767e+09 , 1096.149 , 16.675 , 5.310000e-22 ,    3977.720   ,  4.628 ,7945.525879,  H1, 1080Lines 

Then, we can iterate over the rows of the previous specs dataframe to creata our :ref:`gwdatamanager:GwDataManager` object::

  >>> dama = GwDataManager('Glitches')
  >>> dur = 16                                                       # Time interval around the glitch
  >>> for i, row in specs_df.iterrows():
          print("Importing glitch ",i)
          dama.read_gwdata(row['GPStime']-dur, row['GPStime']+dur,
                           ifo=row['ifo'],
                           m_data_source='gwosc-remote',             # where to fetch the data
                           dts_key='{:.2f}'.format(row['GPStime']))  # use the GPStime as a key for each glitch dataset
     
          for k, el in row.items():                                  # To transfer the glitch metadata to the dataset
              dama['{:.2f}'.format(row['GPStime'])].attrs[k] = el


Let's have a look ot the dataset that we've just created::

  >>> print(dama)
  
  Glitches:
     ├── 1164876401.08
     ├── 1165769137.00
     ├── 1166331693.26
     ├── 1167767137.88
     ├── 1175571109.19
     ├── 1180991604.81
     ├── 1182836167.70
     ├── 1185609100.57
     └── 1186797583.19

  Attributes:
     dama_name : Glitches
    time_stamp : 20-11-23_10h47m15s

and each glitch dataset will have attributes::

  >>> key = dama.groups[0]    # Get the identifier of the first dataset (group)
  >>> dama[key].show_attrs    # Attribute to print the attributes
  
      GPStime : 1164876401.082
      Q-value : 45.255
    amplitude : 3.33e-22
    bandwidth : 7136.748535
    centralFreq : 4382.108
      channel : None
        chisq : 0.0
     chisqDof : 0.0
   confidence : 1.0
     duration : 0.809
           id : QnaUhn1Rdj
          ifo : H1
        label : 1080Lines
     peakFreq : 1075.609
  sample_rate : 4096.0
          snr : 17.352999999999998
           t0 : 1164876385.081787
         unit : 

.. note:: Currently :ref:`index:GWdama` doesn't support to have the same *dama* shared between multiple processes, like it is possible with `h5py and MPI support <https://docs.h5py.org/en/stable/mpi.html#parallel>`_. So, every time one wants to create a :ref:`gwdatamanager:GwDataManager` object, this must be open by only a process at the time. However, this thing can be circumvented creating (and deleting) one independent *dama* for each process. We canimport and pre-process data in parallel with each of them, and then, at the end of the pool collect their data together in the final :ref:`gwdatamanager:GwDataManager`.


Multiple channels, same epoch
=============================

This is one of the typical scenario of Detchar, where one wants to find *correlations* between various channels.
This operation can be typically performed on **Virgo farm machines**, where multiple auxiliarry channel data are available.

The possibility to read multiple channels is natively implemented in :ref:`index:GWdama`, for their data are stored iun the same ``.gwf``, opened when reading its content::

  >>> form gwdama.io import GwDataManager
  
  >>> gps_start = '2020-2-10 19:30' 
  >>> gps_end = '2020-2-10 19:35'
  >>> channels = ['V1:Hrec_hoft_20000Hz', 'V1:LSC_DARM', 'V1:BsX_TX'] # List of channels
  
  >>> dama = GwDataManager('Virgo_farm')
  >>> dama.read_gwdata(gps_start, gps_end, m_channels=channels,
                       m_data_source='virgofarm',                      # where to fetch the data
                       dts_key='Raw',                                  # a key for the group of channels
                       nproc=10, verbose=True,                         # To speed it up and check what's happening
                       resample=2000)                                  # Downsample the data to 2000 Hz to save memory
                       
Notice that only the first two lines of the last command were mandatory. Specifying a ``dts_key`` (which in this case will cover the entire group of data) is convenient to distinguish the imported data from the processed one that will be created shortly. In the last line, the partameter ``resample`` has been used to downsample **all** the raw signals of these channels. This is implemented by means of the FFT method of `scipy.signal.resample <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.resample.html#scipy.signal.resample>`_ (it is important that the original frequency is divisible by the desired resampling frequency). Other methods of resampling will be seen in the :ref:`Data Analysis section <data-analysis>`. If one whanted instead to resample each channel to a different frequency, a dictionary ``{channel: frequency, }`` could have been passed to the ``raesample`` parameter::

  >>> dama.read_gwdata(gps_start, gps_end, m_channels=channels,
                       m_data_source='virgofarm',    
                       dts_key='Raw',                              
                       nproc=10, verbose=True,                      
                       resample={channels[0]:2000, channels[1]:2500})  # Hrec to 2000 Hz, and DARM to 2500 Hz
                       

The ``sample_rate`` attribute of each channel is then set according to this resampling frequency.
                       

.. note:: If not on Virgo farm machines, GWOSC gives access to `500 auxiliary channels <https://git.ligo.org/gwosc-public/auxiliary-channels/-/blob/master/GW170814/dataprep/chanlist.csv>`_ in the three hours around GW170814 (`link here <https://www.gw-openscience.org/auxiliary/GW170814/>`_). The hdf5 file (17.7 GB) can be amazingly opened by :ref:`index:GWdama`. You should download it and extract for ``.tar`` first, then::

    >>> from gwdama.io import GwDataManager
    >>> path_to_file = 'path/to/AUXR_HDF.hf'  
    >>> dama = GwDataManager(path_to_file)




.. _data-analysis:

---------------
 Data analysis
---------------

In this section, we will report some basic examples of analysis of data and preprocessing by menas of  :ref:`index:GWdama`. These are usually intermediate operations to perform right after the raw data has been imported and before saving to disk a version of it that can be passed to the next step of a *pipeline* or to be forwarded to some Mahcine Learning algorithm. In some cases, as for example in noise studies, the main output are usually some plots and certain summary statistics. To this purpose, :ref:`index:GWdama` integrates some methods from `scipy.signal <https://docs.scipy.org/doc/scipy/reference/signal.html>`_ and the excellent `matplotlib <https://matplotlib.org/>`_ Python packages, with some modifications specifically tailored for GW data analysis. **More to come!!**

Integration with GWpy
=====================

In the LIGO and Virgo collaborations, `GWpy <https://gwpy.github.io/docs/stable/index.html>`_ is a widespread Python package, which already provides excellent tools for studying GW data. :ref:`index:GWdama` integrates with it thanks to two methods:

* :func:`~gwdama.io.Dataset.to_TimeSeries` to convert a :class:`~gwdama.io.Dataset` object to a `TimeSeries <https://gwpy.github.io/docs/stable/api/gwpy.timeseries.TimeSeries.html#gwpy.timeseries.TimeSeries>`_ of GWpy;
    
* :func:`~gwdama.io.GwDataManger.from_TimeSeries` to import a `TimeSeries <https://gwpy.github.io/docs/stable/api/gwpy.timeseries.TimeSeries.html#gwpy.timeseries.TimeSeries>`_ or a `TimeSeriesDiuct <https://gwpy.github.io/docs/stable/api/gwpy.timeseries.TimeSeries.html#gwpy.timeseries.TimeSeriesDict>`_ into the current instance of :class:`~gwdama.io.GwDataManager`.

These givese the opportunity to move back and forth between the two, exploiting the tool that better suites for a specific job, and in particular the :ref:`index:GWdama` capabilities of saving well organized datasets.

Example of a `TimeSeriesDiuct <https://gwpy.github.io/docs/stable/api/gwpy.timeseries.TimeSeries.html#gwpy.timeseries.TimeSeriesDict>`_ object created with random data::

    >>> from astropy import units as u
    >>> from gwpy.timeseries import TimeSeries, TimeSeriesDict
    >>> import numpy as np
    >>> from gwdama.io import DwDataManager
    
    >>> size = 1000           # size
    >>> fs = 100             # sample rate
    >>> epc=to_gps('today')   # epoch

    >>> np.random.seed(1234)
    >>> TSdict['Unif'] = TimeSeries(np.random.random(size=size), sample_rate =fs, epoch = epc, unit=u.m)
    >>> TSdict['Gauss'] = TimeSeries(np.random.normal(scale=.5, size=size), sample_rate =fs, epoch = epc, unit=u.V)
    >>> TSdict['Gamma'] = TimeSeries(np.random.gamma(shape = 3, scale =.5, size=size), sample_rate =fs, epoch=epc, unit=u.K)

and then we convert it into a Group inside a :class:`~gwdama.io.GwDataManager` object::

    >>> grp = dama.from_TimeSeries(TSdict, 'fromTS')
    >>> print(dama)
    mydama:
      └── fromTS
          ├── Gamma
          ├── Gauss
          └── Unif

      Attributes:
         dama_name : mydama
        time_stamp : 21-01-05_19h54m21s


Spectral Analysis
=================

One of the most important and common techniques to study of time series is to detect any periodicities this data may exhibit. This is done by means of the *spectral analysis*, consisting in the estimation of the frequency content of a signal from a sequence of time samples of it.

:ref:`index:GWdama` implement several standard methods to do so, plus some extensions specifically tailored for the study of GW data. In this section, we will report some examples concerning the estimation of the *Power Spectral Density* (PSD) of a signal and its *Spectrogram* making use of different methods. Firstly, let's initialize a :class:`~gwdama.io.GwDataManager` object with the strain data of Virgo from 
O2::

  >>> from gwdama.io import GwDataManager
  >>> iport matplotlib.pyplot as plt
  
  >>> dama = GwDataManager('Sample')
  >>> dama.read_gwdata('2017-08-10 12:00', '2017-08-10 12:05',
                       ifo='V1', rate='16k')

Let's assigna a variable to this :ref:`dataset:Dataset` and customize some of its attributes::

  >>> hrec = dama['key']
  >>> hrec.attrs['channel']='V1:Hrec_hoft_16384Hz'
  >>> hrec.attrs['unit']='strain'

.. topic:: Resampling

  In the previous example, we fetched from `online GWOSC <https://www.gw-openscience.org>`_ (default) the data of Virgo strain at their full sampling rate of 16 kHz: ``rate='16k'`` (instead of the default ``rate='4k'``). If I now want to save some memory *downsampling* this signal to a lower value of ``fs``, I can use one of the several methods implemented in :ref:`index:GWdama`. Firstly, the :ref:`dataset:Dataset` object integrates the :func:`~gwdama.io.Dataset.resample` method, which perform downsampling/resampling with three slightly different techniques: Poly-phase filter, fft, or decimation. Refer to the documentaion of :ref:`dataset:Dataset` for further details. For our purposes, we will make use of the default poly-phase technique::

     >>> hrec = hrec.resample(4000, dts_key='Proc/res_4kHz')
     
     >>> print(dama)
     Sample:
        ├── Proc
        │   └── res_4kHz
        └── key

     Attributes:
         dama_name : Sample
        time_stamp : 21-01-12_18h31m48s

     >>> hrec.show_attrs
         channel : V1:Hrec_hoft_16384Hz
     sample_rate : 4000
              t0 : 1186401618.0
            unit : strain

  where we created a new group of "processed data" (``Proc``) with this resampled :ref:`dataset:Dataset`, assigning to it the variable ``hrec`` by means of which we will work in the rest of this section.
  
  Also, when reading data from disk, say from Virgo farm, you can pass the parameter ``resample`` equals to the desired resampling frequency or to a dictionary with channel names as keys and individual resampling frequencies as values, as in this example::
  
    >>> gps_start = '2020-2-10 18:30'
    >>> gps_end = '2020-2-10 18:35'
    >>> channels = ['V1:Hrec_hoft_20000Hz', 'V1:LSC_DARM', 'V1:BsX_TX']
  
    >>> dama = GwDataManager('Vfarm')
    >>> dama.read_gwdata(gps_start, gps_end, m_channels=channels, m_data_source='virgofarm',
                         resample={channels[0]:5000, channels[1]:2500})    # Resample Hrec and DARM
  

Averaged Periodogram - Welch's Method
-------------------------------------

:ref:`dataset:Dataset` objects have the :func:`~gwdama.io.Dataset.psd` method, which is based on `scipy.signal.welch <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.welch.html>`_ function. It allows to specify the duration in seconds of each fft, their overlap, the way to compute the averages (see example below), and whether to return arrays for the frequency and PSD estimate vectors, or to create a new :ref:`dataset:Dataset`. Refer to the documentation for further details.

Let's compare the Amplitude Spectral Dansity (ASD) obtained with the `Welch's method <https://en.wikipedia.org/wiki/Welch%27s_method>`_ [1] of mean averaged, overlapping periodograms, with a modified version of it obtained with median averages instead. We can compute the two with 4 seconds fft's, with 50% (2 seconds) of overlap::

  >>> fw, Sw = hrec.psd(4,2,return_type='array', average='mean')
  >>> fm, Sm = hrec.psd(4,2,return_type='array', average='median')
  
  >>> fig, ax = plt.subplots()
  >>> ax.loglog(fw, Sw**.5, alpha=.7, label="Welch's PSD")
  >>> ax.loglog(fm, Sm**.5, alpha=.7, label="Median PSD")
  >>> ax.set(xlim=(10,2000), xlabel='Frequency [Hz]', ylabel=r"ASD [$1/\sqrt{Hz}$]", title=hrec.attrs['channel'])
  >>> ax.legend()
  >>> plt.show()

.. image:: figures/PSD2.png
   :height: 300px
   :width: 400 px
   :alt: random
   :align: center
   
The slight difference between the two curves sems to highlight the presence of non-stationary noise. More details will follow momentarily.


Multi-Taper Method
------------------

The `Multi-Taper Method <https://en.wikipedia.org/wiki/Multitaper>`_ (MTM) is an alternatival way of estimating the spectral content of a signal due to D.J. Thomson [2]. Like the Welch's method, it is non-parametric, with a slith modification to the classical trade-off between frequency resolution and variance of the estimator. In the Welch's method the latter is determined by the duration of fft and their number; in the MTM this is instead given by the numer of windows to make use of, and it is typicaly suitable for shorter segments of data, ore ones with significant non-stationarities in the baseline noise level.

In the MTM a set of ortho-normal tapers of length ``Kmax`` is obtained by solving the functional variation problem of finding a set of functions maximally concentrated in a frequency interval of width *W* (their PSDs are maximal there), contrained to having a fixed norm, given for example by the *ell-2* metric of the sum of the squares. The optimal *eigen-tapers* are given for example by a `Discrete Prolate Spheroidal Sequence  <https://en.wikipedia.org/wiki/Window_function#DPSS_or_Slepian_window>`_ (DPSS) [3]. Computing a periodogram with each of these as a window function, one obtains a corresponding number of *independent* spectral estimates. Averaging over them yields a variance reduction equal to the numer of windows used, similar to what it is achieved with the Welch's method and multiple segments of data (assumed to be stationary). 

Further datails on this can be found in the corresponding section of :ref:`preprocessing:Spectral Analysis`.

:ref:`index:GWdama` includes this method in the **preprocessing** submodule. Let's compare it with the Welch's method with an example. Firstly, we can select only a small subset, corresponding to a segment duration ``dur``, of the dataset imported above. Then we estimate its PSD with the Multi-Taper method::

  >>> from gwdama.preprocessing import mtm_psd
  
  >>> fs = hrec.attrs['sample_rate']
  >>> offs = 20000                   # Consider data points only from this offset
  >>> dur = 5                        # Duration in seconds of the data interval to use
  >>> N =int(fs)*dur
  >>> x = hrec[offs:offs+N]

  >>> Kmax = 10                      # Maximum number of windows to use
  >>> thr = 1-1e-5                   # Useful to truncate the number of windows and avoid leakage:
                                     # change ths value and notice the 1/f behaviour of the ASD

  >>> fm, Sxx = mtm_psd(x, fs, Kmax=Kmax, eigenthr=thr, nproc=4)

Notice that we also added a threshold parameter, ``thr``, to truncate those windows with *eigenvalues* (concentration ratio) smaller than the this threshold. This is because if a window has a lower concentration, it will not decrase to zero at the edges of the data segment and it will cause a spectral leakage with a characteristic *1/f* scaling in the ASD. If you notice this issues, in particular where the ASD is smaller, try increasing the value of the thrashold towards 1. By default,the number of windows is already truncated, but depending on the data this can be not sufficient. Refer to the documentation in the :ref:`preprocessing:Spectral Analysis` section.

Next, we do similar estimations by means of the Welch's method (built in :ref:`dataset:Dataset`) and a single periodogram, over the same data segment of the previous example, and with a `Kaiser window <https://en.wikipedia.org/wiki/Kaiser_window>`_ similar to the first of the windows used by the MTM::

  >>> from scipy.signal import periodogram
  >>> from scipy.stats.distributions import gamma

  >>> ff, P = hrec.psd(dur, dur/2,return_type='array')
  >>> fp, Pp = periodogram(x = hrec[offs:offs+N], fs=fs, window=("kaiser",NW*np.pi))

  >>> perc = 0.925, 0.025
  >>> fig, ax = plt.subplots(figsize=(7,5))
  >>> ax.loglog(fm,Sxx**.5, alpha=.7, label='MTM',color='C0',zorder=2)
  >>> ax.fill_between(fm, gamma.ppf(perc[0],  Kmax, scale=Sxx/Kmax)**.5,
                      gamma.ppf(perc[1],  Kmax, scale=Sxx/Kmax)**.5, alpha=.2,
                      edgecolor='C0', label='95% C.I.',zorder=1)
  >>> ax.loglog(ff, P**.5, alpha=.7, label="Welch's PSD", color='C3', linewidth=1)
  >>> ax.loglog(fp, Pp**.5, alpha=.3, linestyle='-', color='k',label='Kaiser window', zorder=-1, linewidth=.5)

  >>> ax.set(xlim=(10,1500),ylim=(1e-24,1e-18), xlabel=('Frequency [Hz]'), 
             ylabel=(r"ASD [$1/\sqrt{Hz}$]"), title=hrec.attrs['channel'])
  >>> ax.legend()
  >>> plt.show()

.. image:: figures/MTM1.png
   :height: 300px
   :width: 400 px
   :alt: MTM method
   :align: center

The frequency resolution of the MTM method is equal to ``Kmax`` divided by *N*, the number of data points used; in this case, it corresponds to 2 Hz. Also the variance is influenced by ``Kmax``, or, to be more precise, the *effective* number of windows used if a threshold is applied to reduce the leakage. Visibly, the variance of this MTM is smaller than that of the periodogram and not that larger than that of the Welch's method, making use of the full data available (5 minutes instead of 5 seconds). 

Notice also that in the previous figure we added the 95% confidence interval (C.I.) for a variable distributed as a Gamma with ``Kmax`` as shape parameter and the value of the PSD, divided by ``Kmax`` as scale, as it is expected for Gaussian and stationary data (not white, in general).

.. topic:: Further details on MTM

  While the choice of the fft length, the overlap, the duration of the strides, and the window were determinant for the outcome of the Welch's methd for spectral estimation, in the MTM it is so the choice of the bandwidth (``NhBW``) into wich the various windows are subject to be concentrated, and, related to this, the maximum number of windows to make use of (``Kmax``). This, greatly simplifies the solution of the "conflict" between frequency resolution and variance, still paying attention to possible leackage issues due to a value of ``Kmax`` that is too large. In this section we explore the effect of some of the previous parameters.
  
  .. image:: figures/MTM2.png
   :width: 600 px
   :alt: MTM windows
   :align: center
  
  In the figure above we have reported, for various choices of ``NhBW`` (different colors), the windows corresponding to the first 4 elements of a DPSS sequence (left) and their Fourier transform (right). You can notice that for large value of *k* (the sequence element) the window doesn't decrease to zero at the border of the data interval. This causes a spectral leakage, as discussed in the previous section and as visible in the next figure; notice the dashed line representing the charactersitc leakage scaling as *1/f*.

  .. image:: figures/MTM3.png
   :width: 400 px
   :alt: MTM threshold
   :align: center
   
  To further investigate this, for a *half the normalized banwidth* (``NhBW``) of 5 we have reported the first 15 windws of the DPSS. In the left-hand side plot of the figure below, thes windows are reported with continuous lines those with eigenvalue/concentration ratio grater than the thrashold 0.9999, and with dashed lines the other. Similarly, on the right-hand side plot, the eigenvalues greater than this value are represented by bullet, while those smaller with croces. Also, from the dashed grey region, it is visible how after the first 2``NhBW``-1 windows, their eigenvales start to decrease to zero. However, this is often not sufficint and a more stringent threshold to cut off the risk of leakage should be imposed, as in the example above. 
  
  .. image:: figures/MTM4.png
   :width: 800 px
   :alt: MTM threshold
   :align: center


Spectrograms
------------

:ref:`index:GWdama` includes in the :ref:`preprocessing:Preprocessing` submodule three different ways of computing spectrograms, visual representation of the *spectrum of frequencies* of a signal as it varies with time:

* **avg_spectro** function takes a time series ``x`` as its argument, divides it in segments of duration close to a ``stride`` value (in seconds) passed as an argument to the function. The approximation is made in order to have a number of points compatible with the sampling frequency of the data, the length in seconds of each fft, and their overlap. The very last part of ``x`` may be deleted from the analysis. Then it computes independent PSDs in each of these segments, returning the corresponding series of times (approximately given by the ``stride`` value), frequency series and bi-dimensional time-frequency map. In both this and the next function, the ffts computed in each segment can be average with their means (`Welch's method <https://en.wikipedia.org/wiki/Welch%27s_method>`_ [1]) or with their *medians*, as allowed by the `scipy.signal.welch <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.welch.html?highlight=welch>`_ method. This :ref:`index:GWdama` function also allows every other `Average and Variance statistical function of Numpy <https://numpy.org/doc/stable/reference/routines.statistics.html>`_ that can take an array as a single parameter (but it is not guaranteed that it’ll necessarily make sense, though). E.g.: ``'min'`` and ``'max'`` are legitimate choices (but it is up to you give them a meaningful interpretation). Also, it allows te readily perform *Rayleigh Gaussianity tests*; refer to the next **Note**.

* **multi_spectro** function returns spectral estimates exacly spaced by a value given by the ``stride`` parameter. This comes at the price of deleting, for each segment of data, the very last points, depending on the values of ``stride``, ``fs``, ``fftlength`` and ``overlap``. This is also (usually) faster than the previous one, executing the computation over multiple processes.

* **mtm_spectro** does the same as the previous function, making use of the `Multi-Taper Method <https://en.wikipedia.org/wiki/Multitaper>`_ (MTM) [2] instead of the `Welch's method <https://en.wikipedia.org/wiki/Welch%27s_method>`_ [1]. No data point is lost in each segment to which the data is divided into. This is suitable when a better time resolution is needed, for example because there are non-stationsrities in the data.

Let's consider the same data as in the previous section example, and compute its spectrogram with the three different methods::

  >>> from gwdama.preprocessing import avg_spectro, multi_spectro, mtm_spectro
  
  >>> t, f, S = avg_spectro(hrec.data, 1, .5, 0.25, fs=hrec.attrs['sample_rate'])
  
  >>> fig, ax = plt.subplots()
  >>> ax.pcolormesh(t, f, S**.5, norm='log', shading='gouraud')
  >>> ax.set(xlabel='Time [sec]', ylabel='Frequency [Hz]', yscale='log', ylim=(10,1800))
  >>> cbr = ax.colorbar(cmap='viridis', aspect=40)
  >>> cbr.set_label("Normalized energy", rotation=270, labelpad=15)
  >>> plt.show()
  
.. image:: figures/spectro1.png
   :width: 400 px
   :alt: MTM threshold
   :align: center

With the second method the result is visibly identical::

  >>> tw, fw, Sw = multi_spectro(hrec.data, 1, .5, .25, fs=hrec.attrs['sample_rate'], nproc=4)  # But we can use multiple processes now!

.. image:: figures/spectro2.png
   :width: 400 px
   :alt: MTM threshold
   :align: center

Instead, it is interesting to compare with the rusult obtained with the MTM method::

  >>> tm, fm, Sm = mtm_spectro(hrec.data,1, fs=hrec.attrs['sample_rate'], nproc=4, Kmax=4, eigenthr=0.9999)

.. image:: figures/spectro3.png
   :width: 400 px
   :alt: MTM threshold
   :align: center

This spectrogram has the same variance of the previous ones, given by ``Kmax`` in this case and by the combination of ``stride`` and ``fftlength`` values for the latter, but it has double the frequency resolution: 1 Hz in this case, 2 in the latter. Of course, it was slower to compute. If instead you preferred a smaller variance but the same frequency resolution, you should have run the following command::

  >>> tm, fm, Sm = mtm_spectro(hrec.data,1, fs=hrec.attrs['sample_rate'], nproc=4, Kmax=8, eigenthr=0.9999)

.. topic:: Stationarity: Median-normalized Spectrograms

  The main purpose of spectrograms is to assess the change of the spectral contet of a signal with time. Due to the reach spectral structure of typuical GW data, with magnitudes spanning four or more orders of magnitude in the ASD, besides of *spectral lines*, this information is often not clearly visible from the standard way this quantities are presented. For example, it is not that evident which parts of the previous maps contained non-stationarities. A method to make them to show up better is to normalize the previous spectrograms by the value of the time median of each frequency bin, that is, averagin along the rows::
  
    >>> t, f, S = avg_spectro(hrec.data, 2, .5, 0.25, fs=hrec.attrs['sample_rate'])
    >>> M = S/np.median(S,axis=1)[:,None]

    >>> fig, ax = plt.subplots(figsize=(7,5))
    >>> ax.pcolormesh(t,f,M,shading='gouraud', cmap='Spectral_r', norm='log')
    >>> ax.set(ylabel='Frequency [Hz]', yscale= 'log', xlabel='Time [sec]', ylim=(10,18e2),title=hrec.attrs['channel'])
    >>> cbr = ax.colorbar(aspect=40)
    >>> cbr.set_label("Median-normalized PSD", rotation=270, labelpad=15)
    >>> plt.show()
 
  .. image:: figures/spectro5.png
     :width: 400 px
     :alt: Median normalized
     :align: center  

  If the data were stationary, the values in this map should have been uniformly distributed around 1, corresponding to their median value. This is not the case, and some regions with marked excesses of power: red regions.


.. topic:: The Rayleigh Gaussianity test

  Another desired property for our data is that it is compatible with a *Gaussian distributed process*. Again, since the very complex spectral structure of GW data, with different processes contributing to different parts of the spectrum, we may want to test it at every frequency bin, and possibly over time. Firstly, let us observe that our data is "mostly" Gaussian, that is, integrated over the whole frequency range, it differs only marginally from a Gaussian distribution. We can observe it making use of the ``.hist()`` method of the :ref:`dataset:Dataset` class::
  
    >>> nbins=100                                 # Number of bins in the histogram 
    >>> plot = hrec.hist(log=True, bins=nbins)    # This figure must be re-opened

    >>> from scipy.stats import norm, t, chisquare

    >>> ax = plot.gca()                           # Get the axis inside plot
    >>> ax.patches[-1].set_label('Data')          # Set label for the histogram
    >>> res = np.array([( p.get_x(),p.get_height()) for p in ax.patches])
    >>> x,y = res[:,0], res[:,1]                  # Get a vector of x bins and their values

    >>> ny = norm.pdf(x, *norm.fit(hrec.data))    # Fit with a Gaussian
    >>> ax.plot(x, nx, 'r--', linewidth=1, label=r'Gaussian - $\chi^2 = $ {:.4f}, $p = $ {}'.format(*chisquare(y/y.sum(), ny/ny.sum(), ddof=2)))
    >>> sy = t.pdf(x,*t.fit(hrec.data))           # Fit with a Student
    >>> ax.plot(x, sx, 'b--',linewidth=1, label='Student -- $\chi^2 = $ {:.4f}, $p = $ {}'.format(*chisquare(y/y.sum(), sy/sy.sum(), ddof=3)))
    >>> ax.legend()
    >>> ax.set(xlabel="Strain [dimensionless]", ylim=(1e14,1e18))
    >>> plot.reshow()                             # Re-open the closed figure object
  
  .. image:: figures/hist3.png
     :width: 400 px
     :alt: Median normalized
     :align: center  

  As anticipated, the data, over all the frequency spectrum, are well fitted by a Gaussian distribution. This information is not very useful to us though.
  
  Let's talk about the **Rayleigh Gaussianity test**. This is a *consistency test*, that allows to verify the null hypothesis that the data is compatible with a Gaussian distribution *at all frequencies*, which is the important part. Indeed, if this is the case, the standard deviation of the square amplitude of the Fourier transforms, of duration ``fftlength``, in a ``stride`` of data should be equal to their mean. Instead, if one had to considere the absolute values of the previous Fourier transform, the ratio would have been approximately 0.52. We can implement this test with the functions at our disposal::
  
    >>> fig, (ax0,ax) = plt.subplots(ncols=2, figsize=(9,5),gridspec_kw={'width_ratios': [1, 3]},sharey=True)

    >>> # Rayleigh statistic on the whole data
    >>> _, f, N = avg_spectro(hrec.data, len(hrec)/hrec.attrs['sample_rate'], 1, 0, fs=hrec.attrs['sample_rate'], average='std')
    >>> _, _, D = avg_spectro(hrec.data, len(hrec)/hrec.attrs['sample_rate'], 1, 0, fs=hrec.attrs['sample_rate'], average='mean')

    >>> ax0.semilogy(np.median(N/D,axis=1),f)
    >>> ax0.axvline(1,color='k', linestyle='--',linewidth=1)   # Reference value
    >>> ax0.set(ylabel='Frequency [Hz]', xlabel="R. test statistic")
    >>> ax0.invert_xaxis()

    >>> # Rayleigh spectrogram on strides of 2 second length
    >>> stride = 2

    >>> t, f, N = avg_spectro(hrec.data, stride, .5, 0, fs=hrec.attrs['sample_rate'], average='std')
    >>> _, _, D = avg_spectro(hrec.data, stride, .5, 0, fs=hrec.attrs['sample_rate'], average='mean')

    >>> ax.pcolormesh(t,f,N/D,shading='gouraud', cmap='Spectral_r')
    >>> ax.set(yscale='log', xlabel='Time [sec]', ylim=(10,18e2),title=hrec.attrs['channel'])
    >>> cbr = ax.colorbar(aspect=40)
    >>> cbr.set_label(r"Rayleigh test statistic - $std|X(f)|^2/mean|X(f)|^2$", rotation=270, labelpad=20)
    >>> fig.subplots_adjust(wspace=.018)
    >>> plt.show()

  .. image:: figures/spectro4.png
       :width: 550 px
       :alt: MTM threshold
       :align: center  

  The above figure gives further information on the data with respect to the median normalized spectrogram used to study non-stationarities. It is immediately visible the dark blue band around 50 Hz, the frequency of the electric mains, which in the previous spectrogram appeared to be stationary, while now shows up with values smaller than the expected one for Gaussian data; this means that in this band date is not Gaussian, but just stationary. The same conclusion can be drown for the thin spectral lines between 300 and 400 Hz, which already appeared in the usual spectrograms. The excesses of power at low frequncy has instead values larger than one, meaning that there the standard deviation of their ffts is larger than their mean: large variations of spectral content with respect to that statistically expected for a Gaussian process. 


.. note::  The previous functions to compute the spectrograms are not implemented as methods of the :ref:`dataset:Dataset` class. This is because, usually, these quantities are byproducts not meant to be saved for future use; they can be used as the final producat to show plots, and can be recomputed periodically from the raw data, or its reprocessed versions. However, if you need to store them into a :class:`~gwdama.io.GwDataManager` object, you can manually create the corresponding :ref:`dataset:Dataset` of structured data, which are very convinent. The procedure is the following: 
  
  1. Obtain the numpy arrays corresponding to the time and frequency vectors, and the spectrogram bidimensional map::
    
      >>> t, f, S = avg_spectro(hrec.data, 2, 1, 0.75, fs=hrec.attrs['sample_rate'], t0=hrec.attrs['t0'])    
    
  2. Define a ``dtype`` with fields corresponding to the previous quantities, say ``'time'``, ``'freq'`` and ``'spec'``::

      >>> Sdt = np.dtype([('time', 'f', t.shape), ('freq', 'f', f.shape), ('spec', 'f', S.shape)])   
  
  3. Create a dataset with the values obtained in the first point and with a format defined in the second point::
  
      >>> avgS = dama.create_dataset('Spec/avg', data=np.array((t, f , S), dtype=Sdt)) 
  
  
  Then, the various part of this :ref:`dataset:Dataset` can be accessed passing the *name* of the corresponding field to the dataset handler::
  
    >>> avgS['spec']
    
  to return the 2D array of spectrogram values.

-----------------
 Transformations
-----------------

* Discrete Fourier transform: ASD;
* Q-transform and wavelets

-------------------------

1. Welch, P.D. (1967), "The use of Fast Fourier Transform for the estimation of power spectra: A method based on time averaging over short, modified periodograms." IEEE Transactions on Audio and Electroacoustics, AU-15 (2): 70-73.

2. Thomson, D.J. (1982) "Spectrum estimation and harmonic analysis." Proceedings of the IEEE, 70, 1055-1096.

3. Slepian, D. (1978) "Prolate spheroidal wave functions, Fourier analysis, and uncertainty V: The discrete case."" Bell System Technical Journal, Volume 57, 1371430.