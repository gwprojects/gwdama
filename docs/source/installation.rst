.. highlight:: sh

============
Installation
============

This tutorial will guide you through the process of installation ``gwdama`` Python package. 

``gwdama`` is `available from pip <https://pypi.org/project/gwdama/>`_, so, for the most of the users who want to try
its functionalities, just refer to the instruction in :ref:`install-pip` section.

If you are a developer, you may want to clone the package from gitlab, then experiment with it. In this case, refer to the section :ref:`for-developers`.

.. note:: Currently, the most of the functionalities of this package have been tested on Virgo farm machines or on PC-Universe2 of Unipi. Be sure to have an account on any of them before proceeding. Otherwise, ``gwdama`` can be still used to read `gwosc online data <https://www.gw-openscience.org/data/>`_.


.. _install-pip:

Install with pip
================

``gwdama`` is compatible with verisons of Python >= 3.6. To install it, open your terminal and simply type:
::

    $ pip install gwdama

However, in most scenario, it is recommendable to istall it within a  `python virtual environment <https://docs.python.org/3.6/tutorial/venv.html>`_.

1. Create a Python3 environment called ``env``, or whatever (in this case, remember to substitute it to env in the following commands). This should be done without-pip, which is installed in the next step::

    $ python3 -m venv --without-pip env

2. Activate the environment, which is going to be empty (really!)::
  
      $ source env/bin/activate

  Get pip, setuptools and wheel from the web::

      (env) $ curl https://bootstrap.pypa.io/get-pip.py | python

3. Verify that ``pip`` is updated to its most recent version:::
        
      (env) $ pip install --upgrade pip

4. Then you are ready to install ``gwdama``!!::
    
      (env) $ pip install gwdama

Try to test it with some of the examples in the Quick start guide.

.. warning:: Having an out of date version of ``pip`` may cause an error with ``lalsuite`` package. You can avoid it installing ``lalsuite`` manually before ``gwdama`` or upgrading ``pip`` (recommended!!).

.. _for-developers:

Notes for developers
====================

If you want to contribute to the development of the ``gwdama`` project, you can clone it from gitlab. 

Clone the package
-----------------

In your working directory clone from gitlab the ``gwdama`` repository::

    $ git clone https://gitlab.com/gwprojects/gwdama.git
    
.. note:: If you are a member of the ``gwdama`` project, you may want to `add GitLab keys <https://docs.gitlab.com/ee/ssh/>`_ and clone the project with
    ::
        
        $ git clone git@gitlab.com:gwprojects/gwdama.git
        
    instead. 

This will create a new gwdma folder in your directory. Change to this one and check (all) the available branches::

    $ cd gwdama
    $ git branch -a
    
By default, only the master directory is synchronised. If you want to try different branches, like ``fradev``::

    $ git checkout fradev

At this point, it is usually a good idea to `configure git <https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup>`_ with your email etc. 

Now you are good to go! 

You an test the installation running the ``test_gwdama_gwosc.py`` script in the ``/tests`` folder. **This will be available soon also for non-developer users**

.. topic:: Start a new feature branch

  If you plan to develop this project, in order not to interfere with somebody else's work, it could be a good idea to make *now* your
  own branch out of the current one, say ``fradev``::

    $ git checkout -b myBrandNewBranch fradev

  Make your changes/development, and then commit it::
  
    $ git commit -am "Your message"

  When you feel safe, merge your changes to the original branch (``fradev`` in this case),
  `without a fast-forward <https://stackoverflow.com/questions/6701292/git-fast-forward-vs-no-fast-forward-merge>`_::

    $ git checkout fradev
    $ git merge --no-ff myBrandNewBranch

  Now push changes to the server::

    $ git push origin fradev
    $ git push origin myBrandNewBranch

Install the package
-------------------

Once you have cloned the directories containing ``gwdama``, you have to install it. The creation of the environment is the
same of what seen in the previous example.
The following installation procedure works correctly on Virgo farm machines and PC-Universe2.
Remember to put the resulting ``env`` directory into a ``.gitignore`` file in order to avoid pushing it!

1. Create a Python3 environment called ``env``, or whatever (in this case, remember to substitute it to env in the following commands). This should be done without-pip, which is installed in the next step::

    $ python3 -m venv --without-pip env

2. Activate the environment, which is going to be empty (really!)::
  
      $ source env/bin/activate

  Get pip, setuptools and wheel from the web::

      $ curl https://bootstrap.pypa.io/get-pip.py | python

3. Deactivate and reactivate the environment and check if the previous packages are installed and up-to dated. Check also the versions atc.::

      $ deactivate
      $ source env/bin/activate
      $ python --version
      $ pip list

4. Install the required modules.  The procedure varies depending on whether a requirements.txt file is available (provided somebody has created one with ``pip freeze > requirements.txt``) or not:

  a. Install the packages from the requirements::
        
        $ pip install -r requirements.txt

  b. Install everything manually (not recommended!)::
    
        $ pip install numpy scipy matplotlib pandas jupyter scikit-learn gwpy sphinx sphinx-copybutton
        
    Also, it will be necessary to install ``python-ldas-tools-framecpp`` to read and write ``.gwf`` files::

        $ pip install lalsuite


  Check that the previous steps have been completed successfully: entering the following command shouldn't return any error, warning etc.::

    $ python -c "import numpy, matplotlib, pandas, sklearn, scipy"


  .. note:: For code developing and benchmark tests, it could also be useful to install the line_profiler and memory_profiler packages. These are not included in the requirements.txt file but you can install them easily, within the environment, typing::

      $ pip install line_profiler memory_profiler
    
    Then, you can exploit the IPython megic commands::

      %prun: Run code with the profiler
      %lprun: Run code with the line-by-line profiler
      %memit: Measure the memory use of a single statement
      %mprun: Run code with the line-by-line memory profiler


Install the package (locally)
-----------------------------
.. note::  If you built the environment by means of the ``requirements.txt`` file, and you are planning **not** to modify the module, this step was already inclued there and nothing more is required.

We can install the package locally (for use on our system), and import it anywhere else. Passing the parameter ``-e``, we can install the package with a *symlink*, so that changes to the source files will be immediately available to other users of the package on our system. From the main directory containing the package::

  $ pip install -e .

Done! You are all set up now, and rady to go testing some jupyter notebook. 


Testing the installation
========================

Execute the ``test_gwdama_gwosc.py`` in the ``/tests`` folder to verify the correct installation of the package. This file reads from a csv file the information about some glitches and try to read its data. If the procedure concluded correctly, you should see in the same folder a ``test_glitch_gwosc_cvmfs_out.h5`` file.

.. note:: Some of the glitches listed in the csv file are not present on GWOSC. This is prompted by a WARNING message, but doesn't cause the script to interrupt its iterations.
