"""Create, manipulate, read, and write time-series data with a customised Virgo class

"""
from .gwdatamanager import GwDataManager