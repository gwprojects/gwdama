"""
This file is used by the GwDataManager.read_from_virgo() method. It contains
the paths of common ffl in the Virgo Data repository.
It should be kept up to date with the location of the various ffl files.

"""

# ----- Virgo -----
V1_raw = "/virgoData/ffl/raw.ffl"
V1_trend = "/virgoData/ffl/trend.ffl"
V1_trend100 = "/virgoData/ffl/trend100s.ffl"


# ----- LIGO -----
L1_newer = "/virgoData/ffl/fromCIT/L1Online.ffl"
H1_newer = "/virgoData/ffl/fromCIT/H1Online.ffl"

L1_older  = "/data/prod/hrec/L1Online/longFiles.ffl"
H1_older  = "/data/prod/hrec/H1Online/longFiles.ffl"

# ----- O3a -----
V1_O3a = "/virgoData/ffl/O3A/V1Online.ffl" # To be updated with V1O3ARepro1A.ffl or V1O3ARepro1A_raw.ffl
H1_O3a = "/virgoData/ffl/O3A/H1Online.ffl"
L1_O3a = "/virgoData/ffl/O3A/L1Online.ffl"

# ----- UniPi -----
unipi_O3 = "/virgoData/ffl/unipi_O3.ffl"         # for rawdata buffer copy
unipi_arch = "/virgoData/ffl/unipi_rawarch.ffl"    # for rawdata archive copy
