"""
Functions to make nice plots
"""
from .plot import make_hist, reshow