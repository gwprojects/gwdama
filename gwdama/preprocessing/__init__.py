"""
Some useful pre-processing functions, like whitening a wavelte transforms
"""

from .preprocessing import whiten, taper, avg_spectro, multi_spectro, next_power_of_2, decimate_recursive, mtm_psd, mtm_spectro