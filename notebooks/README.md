# This is a list of jupyter notebooks with examples
**Notice that for the currently manintained version of GwDataManager only Example 4 is active**
- [Example_1](Example1_VirgoFarm.ipynb): basic usage of `GwDataManager` class, including GWpy `TimeSeriesDict`,
and basic plotting;
- [Example_2](Example2_PCuniverse2.ipynb): basic usage of `GwDataManager` class, on PC Universe 2;
- [Example_3](Example3_glitches.ipynb): glitches;
- [Example_4](Example4_newGwDataManager): current version of `GwDataManager`. 

## ToDos
- **Bottleneck**: reading a frame file list (and temporarily storing it as a dictionary)
is highly inefficient and memory (and time) consuming. Now the gwf paths are stored for once in the corresponding attribute, and this should make data acquisition faster, avoiding to repeat the creaton of the paths dictionsry;
- Improvement: reading local LIGO data less recent that one hour is quite inefficien since 10000 seconds gwf files should be read. Improve on this parallelising etc.;
- Improvement: add a method to convert the GWpy `TimeSeriesDict` to a Pandas `DataFrame`, more suitable for Machine Learning manipulation. Before that:
    - All the channels should have the same sampling rate in order to be stored in the same
    `DataFrame`. This can be easily done with the `resample` method of GWpy's TS, provided
    the frequency is a multiple of $2$ (as a consequence of using the fft method for resampling);
    - Part of the metadata should be thrown away (like the units); 
- Improvement: **multiprocessing** is already implemented, reading from multiple `.gwf` files
at the same time, for the case of stretches of data $\gtrsim 100$ seconds. It should be checked
whether multiple channels are read sequentially or in parallel too. Otherwise, for shorter stretches
of data, a parallelization over the channels should  be implemented.

